#!/usr/bin/perl

=head1 NAME

create_core_pan_genome_all.pl

=head1 SYNOPSIS

  create_core_pan_genome_all.pl min_cluster_size clstr2id clstr2ct

Options:
 
   none.

=head1 DESCRIPTION

This script defines core and flexible protein clusters (PCs) between metagenomes.  The core metagenome is defined as PCs that are present in all k metagenomes, while the flexible metagenome is the total unique PCs found in k genomes. Data were calculated as all variations of n choose k: n!/k!(n - k)!.
    
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

if ( @ARGV != 3 ) { die "usage: prg cluster_size clstr2id clstr2ct" }

use strict;

my $cluster_size = shift @ARGV;
my $clstr2id     = shift @ARGV;
my $clstr2ct     = shift @ARGV;

my %get_clusters;
my %id_to_cluster;

open( HC,  "$clstr2id" );
open( CCT, "$clstr2ct" );

while (<CCT>) {
    chomp $_;
    my ( $cluster, $ct ) = split( /\t/, $_ );
    $cluster =~ s/ /_/;
    if ( $ct >= $cluster_size ) {    # high confidence
        $get_clusters{$cluster} = $ct;
    }
}

while (<HC>) {
    chomp $_;
    my ( $cluster, $id ) = split( /\t/, $_ );
    $cluster =~ s/ /_/;
    if ( exists $get_clusters{$cluster} ) {
        $id_to_cluster{$id} = $cluster;
    }
}

my %cluster_sample_ct;

my $dir =
"/rsgrps1/mbsulli/bonnie/projects/protein_universe/read-to-20110913-prot-universe/tophits";
my $out = "all";
opendir( my $dh, $dir ) || die;
my @samples = (
    "SIO_TFF_CsCl",                      "SIO_FeCl_DNase",
    "SIO_FeCl_CsCl",                     "SIO_FeCl_Sucrose",
    "Mbari_1_FeCl3_Coastal_10m",         "Mbari_2_FeCl3_Up-Meso_10m",
    "LineP_8_Feb_2009_FeCl3_P26_10m",    "Mbari_6_FeCl3_Oc-Oligo_1000m",
    "LineP_12_Jun_2009_FeCl3_P4_500m",   "LineP_9_Feb_2009_FeCl3_P26_500m",
    "LineP_10_Feb_2009_FeCl3_P26_1000m", "LineP_16_Jun_2009_FeCl3_P12_1000m",
    "LineP_11_Feb_2009_FeCl3_P26_2000m", "Mbari_7_FeCl3_Oc-Oligo_4300m",
    "Mbari_5_FeCl3_Oc-Oligo_DCM-105m",   "Mbari_4_FeCl3_Oc-Oligo_10m",
    "LineP_7_June_2009_FeCl3_P26_1000m", "LineP_1_Aug_2009_FeCl3_P26_10m",
    "LineP_15_Jun_2009_FeCl3_P12_500m",  "LineP_13_Jun_2009_FeCl3_P4_1000m",
    "LineP_4_Aug_2009_FeCl3_P26_2000m",  "LineP_13_June_2009_FeCl3_P4_10m",
    "LineP_14_June_2009_FeCl3_P12_10m",  "LineP_14_Jun_2009_FeCl3_P4_1300m",
    "LineP_5_June_2009_FeCl3_P26_10m",   "LineP_8_June_2009_FeCl3_P26_2000m",
    "LineP_3_Aug_2009_FeCl3_P26_1000m",  "dunk_3",
    "LineP_17_Jun_2009_FeCl3_P12_2000m", "fitzroy2",
    "LineP_2_Aug_2009_FeCl3_P26_500m",   "Mbari_3_FeCl3_Up-Meso_DCM-42m"
);

my %cluster_to_count;
while ( my $file = readdir $dh ) {
    if ( $file =~ /.fa.bl2pu/ ) {
        open( F, $file ) || die;
        my $sample = $file;
        $sample =~ s/.fa.bl2pu//;

        #print "$sample\n";
        while ( my $q = <F> ) {
            chomp $_;
            my @fields = split( /\t/, $q );
            my $read   = $fields[0];
            my $hit    = $fields[2];
            if ( exists $id_to_cluster{$hit} ) {
                my $cluster = $id_to_cluster{$hit};
                $cluster_to_count{$cluster}++;
                $cluster_sample_ct{$cluster}{$sample}++;
            }
        }
    }
    else {

        #print "no match $file\n";
    }
}
closedir $dh;

open( OUT, ">cluster2readct2" ) || die;

my %total_in_cl_by_sample;
for my $s (@samples) {
    $total_in_cl_by_sample{$s} = 0;
}

my %shared;
my %not_shared;
my %clusters;
for my $s (@samples) {
    my $count = 1;
    my %pc    = ();

    # layer in the PC for sample
    for my $c ( keys %cluster_sample_ct ) {
        if ( exists $cluster_sample_ct{$c}{$s} ) {
            $pc{$c} = 1;
            push( @{ $clusters{$c} }, $s );
        }
    }

    # now layer in each sample successively and count the shared vs
    # not shared after adding in each sample
    for my $sample (@samples) {
        if ( $sample ne $s ) {
            $count++;

            #layer in the next sample to pc
            for my $c ( keys %cluster_sample_ct ) {
                if ( exists $cluster_sample_ct{$c}{$sample} ) {
                    if ( exists $pc{$c} ) {
                        $pc{$c}++;
                    }
                    else {
                        $pc{$c} = 1;
                    }
                }
            }

            # now count how many of the PC are shared versus not shared
            my $sh  = 0;
            my $nsh = 0;
            for my $p ( keys %pc ) {
                my $ct = $pc{$p};
                if ( $ct >= $count ) {
                    $sh++;
                }
                else {
                    $nsh++;
                }
            }

            # add these data for this count
            $shared{$count}{$s}     = $sh;
            $not_shared{$count}{$s} = $nsh;
        }
    }
}

open( S, ">$out.shared2.txt" );
for my $ct ( sort { $a <=> $b } keys %shared ) {
    my @values = ();
    for my $s (@samples) {
        my $val = $shared{$ct}{$s};
        push( @values, $val );
    }
    print S "$ct\t", join( "\t", @values ), "\n";
}

for my $ct ( sort { $a <=> $b } keys %not_shared ) {
    my @values = ();
    for my $s (@samples) {
        my $val = $not_shared{$ct}{$s};
        push( @values, $val );
    }
    print S "$ct\t", join( "\t", @values ), "\n";
}

open( A, ">all_clusters_to_samples2" );

for my $c ( keys %clusters ) {
    my $count = @{ $clusters{$c} };
    print A "$count\t$c\t", join( ",", @{ $clusters{$c} } ), "\n";
}

