#$ -S /bin/bash

##  Run vmatch
## written by Bonnie Hurwitz

# create the suffix array for this data set
cd $FADIR
/home/bhurwitz/biotools/vmerstat64/mkvtree -db $FILE -pl -allout -smap /home/bhurwitz/bioinfo/biotools/vmatch.distribution/vmatch_symbol_map -v
/home/bhurwitz/biotools/vmerstat64/vmerstat -indexname $FILE.vm -minocc 2 -counts $FILE

export MERFILE="$FILE.vm"
export MER=20

cd $FINALDIR

VMCONF="$TMPDIR/vmersearch.conf"
VMATCH="/home/bhurwitz/biotools/vmerstat64/vmersearch"

#create the conf file
echo "$FADIR/$MERFILE" > $VMCONF
echo "$VMATCH" >> $VMCONF
echo "-strand fp" >> $VMCONF
echo "-output qseqnum qpos counts sequence" >> $VMCONF

# run the MDR filter
FA="$FADIR/$FILE"
MDRREAD="$FINALDIR/results/mdr_prefilter"

# here we create bins of sequences by their mode frequency
$SCRIPTS/repeater_vmersearch_oligos.pl -c $VMCONF -f $FA -n $MER -o $MDRREAD

# now we need to filter the reads so that they are binned into groups by freq
# now filter the reads so that only seqs with 2+ 20mers are present
$SCRIPTS/get_over_ct.pl $MDRREAD/$FILE.mode $MDRREAD/$FILE.bin
$SCRIPTS/get_list_from_fa.pl $MDRREAD/$FILE.masked $MDRREAD/$FILE.bin $MDRREAD/$FILE.bin 

