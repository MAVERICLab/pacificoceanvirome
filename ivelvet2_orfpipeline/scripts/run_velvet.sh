#!/bin/-bash
#$ -S /bin/bash

# for velvet 
PROGRAM="/home/bhurwitz/bioinfo/biotools/velvet_1.0.15/velveth"
PROGRAM2="/home/bhurwitz/bioinfo/biotools/velvet_1.0.15/velvetg"
OUTDIR="$CWD/results/velvet"
SCRIPTS="$CWD/scripts"
HASHLEN=29

## assemble by bin
## iteratively assembles sequences from bins with highest
## to lowest frequency of mers per sequence
cd $CWD
OUT1="$OUTDIR/$FILE"
mkdir $OUT1
ADD="$OUT1/add_fasta.fa"
touch $ADD
for bin in 20 18 16 14 12 10 8 6 4 2; do
  echo $bin
  OUT="$OUT1/$bin"
  mkdir $OUT
  MDRREAD="$CWD/results/mdr_prefilter"
  IN="$MDRREAD/$FILE.bin.$bin.fa"
  cp $IN $OUT
  IN2="$OUT/$FILE.bin.$bin.fa"
  echo "$PROGRAM $OUT $HASHLEN -fasta $IN2 -long"
  $PROGRAM $OUT $HASHLEN -fasta -long $IN2
  $PROGRAM2 $OUT -long_mult_cutoff 1 -unused_reads yes -amos_file yes -min_contig_lgth 100
  cat $OUT/contigs.fa > $OUT/to_rename.fa
  $SCRIPTS/simple_rename.pl $bin $OUT/to_rename.fa $OUT/renamed.fa 
  cat $OUT/renamed.fa >> $OUT1/contigs.fa
done

