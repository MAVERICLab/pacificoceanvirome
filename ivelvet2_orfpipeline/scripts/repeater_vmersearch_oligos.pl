#! /opt/rocks/bin/perl 

=head1 NAME

repeater_vmersearch_oligos.pl

=head1 SYNOPSIS

  repeater_vmersearch_oligos.pl 

Options:

          -c is the config file
          -f is the fasta file of interest
          -o output dir
          -n is the kmer size

=head1 DESCRIPTION

  Purpose: Query an input set of oligomers
  against a kmer suffix array database using vmersearch
  and generate a tab delimited file with a
  count for every oligo.

  Note that suffix array generation via mkvtree and vmerstat
  must be done before the use of this script
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

# use the following modules
use Getopt::Std;
use Bio::Seq;
use Bio::SeqIO;
use strict;
use lib
'/home/bhurwitz/bioinfo/perlcode/modules/Statistics-Descriptive-Discrete-0.07/blib/lib';
use Statistics::Descriptive::Discrete;

# get inputs
my %opts = ();
getopts( 'c:f:o:n:', \%opts );
my $conf   = $opts{'c'};
my $fasta  = $opts{'f'};
my $nmer   = $opts{'n'};
my $outdir = $opts{'o'};

# read fasta file and then reduce to only its name
my $seqin = Bio::SeqIO->new( -format => 'Fasta', -file => "$fasta" );

my $fasta_name;
if ( $fasta =~ /\//g ) {
    $fasta =~ m/.*\/(.*)$/;
    $fasta_name = $1;
}

else {
    $fasta_name = $fasta;
}

# parse the configuration
my @configuration = &configure($conf);
my $index         = shift @configuration;
my $vmersearch    = shift @configuration;

# do the search
print "Doing the vmersearch\n";
&search( $vmersearch, $fasta, $fasta_name, $index, $outdir, \@configuration );

# parse the vmersearch data
# here we will find the mode frequency for each sequence too
print "Parsing the vmersearch search\n";
my %counts;
my %for_mode_plus;
my %for_mode_minus;
my %mask;
open( OUT, "$outdir/$fasta_name.repeats" );
my $last_seqoffset = 0;
LINE:

while ( my $line = <OUT> ) {
    chomp $line;
    next if ( $line =~ /^\#/ );

    ( my $seqoffset, my $meroffset, my $counts ) =
      split( /\t/, $line );

    # get plus direction only for analysis
    if ( $meroffset =~ /\+/ ) {
        if ( $counts < 1000 ) {
            push( @{ $for_mode_plus{$seqoffset} }, $counts );
        }
        else {
            my $start = $meroffset;
            $start =~ s/\+//;
            my $end = $start + 20;
            for my $i ( $start .. $end ) {
                $mask{$seqoffset}{$i} = 1;
            }
        }
    }
    if ( $meroffset =~ /\-/ ) {
        if ( $counts < 1000 ) {
            push( @{ $for_mode_minus{$seqoffset} }, $counts );
        }
        else {
            my $start = $meroffset;
            $start =~ s/\-//;
            my $end = $start + 20;
            for my $i ( $start .. $end ) {
                $mask{$seqoffset}{$i} = 1;
            }
        }
    }

    $counts{$seqoffset} += $counts;
}
close(OUT);

my %mode;
for my $seqnum ( keys %counts ) {

    # get the mode frequency for the read
    my $mode_read_freq_plus     = 0;
    my $mode_read_freq_plus_ct  = 0;
    my $mode_read_freq_minus    = 0;
    my $mode_read_freq_minus_ct = 0;
    my $mode_read               = 0;
    if ( exists $for_mode_plus{$seqnum} ) {
        $mode_read_freq_plus_ct++;
        my $stats1 = Statistics::Descriptive::Discrete->new;
        $stats1->add_data( @{ $for_mode_plus{$seqnum} } );
        $mode_read_freq_plus = int( $stats1->mode() );
    }

    if ( exists $for_mode_minus{$seqnum} ) {
        $mode_read_freq_minus_ct++;
        my $stats2 = Statistics::Descriptive::Discrete->new;
        $stats2->add_data( @{ $for_mode_minus{$seqnum} } );
        $mode_read_freq_minus = int( $stats2->mode() );
    }
    if (   ( $mode_read_freq_plus_ct >= $mode_read_freq_minus_ct )
        && ( $mode_read_freq_plus_ct != 0 ) )
    {
        $mode_read = $mode_read_freq_plus;
    }
    if ( $mode_read_freq_minus_ct > $mode_read_freq_plus_ct ) {
        $mode_read = $mode_read_freq_minus;
    }

    # use the mode for the read direction with the greatest number of
    # mers
    $mode{$seqnum} = $mode_read;

}

# parse and combine with the fasta data
open( MASK, ">$outdir/$fasta_name.masked" );
open( MODE, ">$outdir/$fasta_name.mode" );
my $seqin = Bio::SeqIO->new( -format => 'Fasta', -file => "$fasta" );
my $counter = 0;
while ( my $sequence_obj = $seqin->next_seq() ) {
    my $id  = $sequence_obj->display_id();
    my $seq = $sequence_obj->seq();
    my @s   = split( //, $seq );
    my $c   = 0;
    my @n;
    for my $ss (@s) {
        if ( exists $mask{$counter}{$c} ) {
            push( @n, "N" );
        }
        else {
            push( @n, $ss );
        }
        $c++;
    }
    my $maskseq = join( "", @n );
    $maskseq =~ s/^N+//;
    $maskseq =~ s/N+$//;
    my $seq_len = length $maskseq;
    if ( ( $seq_len >= 50 ) && ( $maskseq !~ /NNNNNNNNNNNN/ ) ) {
        print MASK ">$id\n$maskseq\n";

        # only put seqs if they are big enough after trimming
        if ( exists( $counts{$counter} ) ) {
            print MODE "$id\t$counter\t$mode{$counter}\n";
        }
        else {
            print MODE "$id\t$counter\t0\n";
        }
    }
    $counter++;
}
close(MASK);
close MODE;

#`rm -rf $outdir/$fasta_name.repeats`;

##SUBS##

# run vmersearch
sub search {
    my $vmersearch = shift;
    my $fasta      = shift;
    my $fasta_name = shift;
    my $index      = shift;
    my $outdir     = shift;
    my $conf       = shift;
    my @conf       = @$conf;

    # build the command string
    my $command = "$vmersearch";
    foreach my $option (@conf) {
        $command .= " $option";
    }

    # input query file and give the suffix array database
    $command .= " $index";
    $command .= " $fasta";

    #issue the command
    print "$command > $outdir/$fasta_name.repeats";
    `$command > $outdir/$fasta_name.repeats`;

    return (1);
}

# parse the configuration of vmatch and mkvtree
sub configure {
    my $conf = shift;

    # parse the conf file
    open( conf, "$conf" );
    my @conf;
    while ( my $line = <conf> ) {
        chomp $line;
        push( @conf, $line );
    }
    close(conf);
    return (@conf);
}
