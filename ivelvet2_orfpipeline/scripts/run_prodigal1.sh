#!/bin/-bash
#$ -S /bin/bash

cd $CWD

# for prodigal
PROGRAM="/home/bhurwitz/bioinfo/biotools/prodigal_source/prodigal-read-only/prodigal"
INDIR="$CWD/results/velvet"
OUTDIR="$CWD/results/prodigal-assembly"
SCRIPTS="$CWD/scripts"

# run gene finding
QUERY="$INDIR/$FILE/contigs.fa"
OUT="$OUTDIR/$FILE"
mkdir $OUT
AA="$OUT/aa"
AASIXTY="$OUT/aa.60"
NUC="$OUT/nuc"
OUTPUT="$OUT/out"
SCORE="$OUT/score"
$PROGRAM -a $AA -d $NUC -i $QUERY -o $OUTPUT -s $SCORE -f gff -p meta
$SCRIPTS/filter_by_size.pl $AA $AASIXTY
