#!/bin/-bash
#$ -S /bin/bash

cd $CWD

# for renaming 
AA1="$CWD/results/prodigal-assembly/$FILE/aa.60"
AA2="$CWD/results/prodigal-reads/$FILE/aa.60"
OUTDIR="$CWD/results/rename-orfs/$FILE"
MAP="$FILE.map"
FA="$FILE"

mkdir $OUTDIR

SCRIPTS="$CWD/scripts"

# run gene finding
$SCRIPTS/rename.pl $AA1 $AA2 $OUTDIR $MAP $FA $DATE

