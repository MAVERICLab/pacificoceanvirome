#!/bin/-bash
#$ -S /bin/bash

cd $CWD

# for cdhit 
PROGRAM="/home/bhurwitz/bioinfo/biotools/cd-hit-v4.3-2010-10-25/cd-hit"
INDIR="$CWD/results/rename-orfs/$FILE"
OUTDIR="$CWD/results/self-cluster/$FILE"
SCRIPTS="$CWD/scripts"
IDEN="1"
COV="0.7"
OPTIONS="-g 1 -n 4 -d 0 -T 0 -M 15000"

mkdir $OUTDIR

# run self clustering for each sample in the set
INFILE="$INDIR/$FILE"
OUT="$OUTDIR/cdhit100"

# run cdhit
$PROGRAM -i $INFILE -o $OUT -c $IDEN -aS $COV $OPTIONS

# get 2+ id clusters and cluster to count FILE
OUTCL="$OUTDIR/cdhit100.clstr"
OUTFILE="$OUTDIR/cdhit100"
$SCRIPTS/get_2+_clusters.pl $OUTCL $OUTFILE 

# create a list of ids that belong to the 2+ member clusters
$SCRIPTS/create_id_to_clst.pl $OUTCL $OUTFILE 

# create a fasta FILE with sequences from 2+ member clusters
LIST="$OUTFILE.clstr2id"
OUTFA="$OUTFILE.2+.fa"
$SCRIPTS/get_list_from_fa.pl $OUTFILE $LIST $OUTFA  
