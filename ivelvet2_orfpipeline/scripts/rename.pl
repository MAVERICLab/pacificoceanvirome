#! /opt/rocks/bin/perl -w

=head1 NAME

rename.pl

=head1 SYNOPSIS

  rename.pl AA1 AA2 OUTDIR MAP FA DATE

Options:
 
   none.

=head1 DESCRIPTION

renames sequence ids by the checksum
    
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut


use strict;
use Bio::Seq;
use Bio::SeqIO;
use Digest::MD5;

# $SCRIPTS/rename.pl $AA1 $AA2 $OUTDIR $MAP $FA $DATE
if ( @ARGV != 6 ) { die "Usage: rename.pl AA1 AA2 OUTDIR MAP FA DATE\n"; }

my $aa1 = shift @ARGV;
my $aa2 = shift @ARGV;
my $outdir = shift @ARGV;
my $map = shift @ARGV;
my $fa = shift @ARGV;
my $date = shift @ARGV;

open(MAP, ">$outdir/$map") || die "Cannot open map file\n";
open(OUT, ">$outdir/$fa") || die "Cannot open fa file\n";

my @files = ($aa1, $aa2);

for my $file (@files) {
   my $seqin  = Bio::SeqIO->new( '-format' => 'Fasta', -file => $file );
   
   # assign a unique identifier
   while ( ( my $seqobj = $seqin->next_seq() ) ) {
       my $id = $seqobj->id();
       my $sequence = $seqobj->seq();
       my $name = $date . '_' . $fa . '_' . $sequence;
       my $ctx = Digest::MD5->new;
       $ctx->add($name);
       my $newname = $ctx->hexdigest;
       print MAP "$newname\t$fa\t$date\t$id\n";
       print OUT ">$newname\n$sequence\n"; 
   }
}

