#! /opt/rocks/bin/perl -w

=head1 NAME

get_list_from_fa,pl

=head1 SYNOPSIS

  get_list_from_fa.pl input.fa list output  

Options:
 
   none.

=head1 DESCRIPTION

   to get a list of sequences from a fasta file and put in
   new files by bin. (see in code).

=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;
use Bio::SeqIO;

if ( @ARGV != 3 ) {
    die "Usage: get_list_from_fasta.pl input.fa list output\n";
}

my @bins = (2,4,6,8,10,12,14,16,18,20);
my $inputfilename  = shift @ARGV;
my $list           = shift @ARGV;
my $outputfilename = shift @ARGV;

for my $bin (@bins) {

  my $in = Bio::SeqIO->new(
      '-file'   => "$inputfilename",
      '-format' => 'Fasta'
  );
  my $out = Bio::SeqIO->new(
      '-file'   => ">$outputfilename.$bin.fa",
      '-format' => 'Fasta'
  );
  
  open( LIST, "$list.$bin" ) || die "Cannot open list file: $list.$bin\n";
  my %list;
  my $list_count = 0;
  while (<LIST>) {
      chomp $_;
      $list_count++;
      $list{$_} = $_;
  }
  
  # go through each of the sequence records in the fasta file 
  while ( my $seq = $in->next_seq() ) {
      my $id = $seq->id();
      if ( exists $list{$id} ) {
          $out->write_seq($seq);
      }
  }
}
