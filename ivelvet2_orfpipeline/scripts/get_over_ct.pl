#!/usr/bin/perl

=head1 NAME

get_over_ct.pl

=head1 SYNOPSIS

  get_over_ct.pl in out 

Options:
 
   none.

=head1 DESCRIPTION

   This script finds all sequences that have a kmer count over a certain size.

   in file is a list of id to mer count (mode for a set of sequences) 
  
   bins of sizes are: (2,4,6,8,10,12,14,16,18,20)

   and can be adjusted in the script.

=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;

if ( @ARGV != 2 ) { die "Usage: prg in out\n"; }

my $in  = shift @ARGV;
my $out = shift @ARGV;
open( IN, $in );

my @bins = ( 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 );

my %counts;
while (<IN>) {
    chomp $_;
    my ( $id, $merid, $ct ) = split( /\t/, $_ );
    for my $bin (@bins) {
        if ( $ct >= $bin ) {
            push( @{ $counts{$bin} }, $id );
        }
    }
}

foreach my $bin ( sort { $a <=> $b } keys %counts ) {
    open( OUT, ">$out.$bin" );
    for my $id ( @{ $counts{$bin} } ) {
        print OUT "$id\n";
    }
    close OUT;
}
