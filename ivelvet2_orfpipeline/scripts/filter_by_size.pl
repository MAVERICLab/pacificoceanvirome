#! /opt/rocks/bin/perl -w

=head1 NAME

filter_by_size.pl

=head1 SYNOPSIS

  filter_by_size.pl input.fa output.fa 

Options:
 
   none.

=head1 DESCRIPTION

   get only sequences that are at least 60AA in length
    
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut


use strict;
use Bio::Seq;
use Bio::SeqIO;

if ( @ARGV != 2 ) { die "Usage: filter_by_size.pl input.fa output.fa\n"; }

my $input  = shift @ARGV;
my $output = shift @ARGV;

my $seqin  = Bio::SeqIO->new( '-format' => 'Fasta', -file => $input );
my $seqout = Bio::SeqIO->new( '-format' => 'Fasta', -file => ">$output" );

while ( ( my $seqobj = $seqin->next_seq() ) ) {

    # check seq length 
    my $sequence = $seqobj->seq();
    my $length   = length($sequence);
    if ( $length >= 60 ) {
        $seqout->write_seq($seqobj);
    }
}

