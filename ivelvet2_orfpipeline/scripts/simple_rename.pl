#! /opt/rocks/bin/perl -w

=head1 NAME

simple_rename.pl

=head1 SYNOPSIS

  simple_rename.pl bin in.fa out.fa 

Options:
 
   none.

=head1 DESCRIPTION

renames the sequences in a fasta file by bin and using the md5 checksum
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut


use strict;
use Bio::Seq;
use Bio::SeqIO;
use Digest::MD5;

# $SCRIPTS/simple_rename.pl bin in.fa out.fa 
if ( @ARGV != 3 ) { die "Usage: simple_rename.pl bin in.fa out.fa\n"; }

my $bin = shift @ARGV;
my $in = shift @ARGV;
my $out = shift @ARGV;

open(OUT, ">$out") || die "Cannot open out file\n";

   my $seqin  = Bio::SeqIO->new( '-format' => 'Fasta', -file => $in );
   
   # assign a unique identifier
   while ( ( my $seqobj = $seqin->next_seq() ) ) {
       my $id = $seqobj->id();
       my $sequence = $seqobj->seq();
       my $name = $sequence;
       my $ctx = Digest::MD5->new;
       $ctx->add($name);
       my $newname = $ctx->hexdigest;
       $newname = $bin . "_" . $newname;
       print OUT ">$newname\n$sequence\n"; 
   }

