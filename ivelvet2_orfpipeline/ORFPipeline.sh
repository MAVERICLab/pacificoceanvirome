#!/bin/sh

## This pipeline runs the following steps 
## 
##  1 - Assemble the sequences by sample for the 454 data (using velvet) 
##  2 - Call ORFs using prodigal on assembly
##  3 - Call ORFs using prodigal on reads
##  4 - rename ORFs systematically 
##  5 - Cluster all ORFs within this set for the blastdbs (cdhit)

## 1 - Assemble the sequences by sample for the 454 data (using velvet)
## Date = year - month - day
## There can be multiple files here by sample
DATE="20110102"
FINALDIR="/home/bhurwitz/bioinfo/protein_universe/ivelvet2/$DATE"
FADIR="/home/bhurwitz/bioinfo/data/processed/$DATE"
SCRIPTS="$FINALDIR/scripts"
cd $FINALDIR
export CWD=$PWD
export DATE=$DATE
export FADIR=$FADIR
export SCRIPTS=$SCRIPTS
export FINALDIR=$FINALDIR
COUNT=0

cd $FADIR
for file in `ls *fa`; do
   COUNT=`expr $COUNT + 1`
   export FILE=$file
   echo "$file"

   #  0 - mdr prefilter the sequences to have at least one 20mer > 2 in suf arr
   NAME0="aa.$COUNT.$DATE"
   qsub -pe pthreads 8 -N $NAME0 -e $FINALDIR/err0 -o $FINALDIR/out0 -v FINALDIR,SCRIPTS,FILE,CWD,DATE,FADIR $SCRIPTS/run_vmatch.sh | sed 's/Your job //' | sed 's/ .*//'  > $CWD/$NAME0

   #  1 - Assemble the sequences by sample for the 454 data (using velvet)
   # Your job 21013 ("a.1.20100220") has been submitted
   NAME1="a.$COUNT.$DATE"
   qsub -hold_jid `cat $CWD/$NAME0` -pe pthreads 8 -N $NAME1 -e $FINALDIR/err1 -o $FINALDIR/out1 -v FILE,CWD,DATE,FADIR $SCRIPTS/run_velvet.sh | sed 's/Your job //' | sed 's/ .*//'  > $CWD/$NAME1 
   
   #  2 - Call ORFs using prodigal on assembly, remove ORFs that are < 60AA
   NAME2="b.$COUNT.$DATE"
   qsub -hold_jid `cat $CWD/$NAME1` -pe pthreads 8 -N $NAME2 -e $FINALDIR/err2 -o $FINALDIR/out2 -v CWD,FILE $SCRIPTS/run_prodigal1.sh | sed 's/Your job //' | sed 's/ .*//' > $CWD/$NAME2

   #  3 - Call ORFs using prodigal on reads, remove ORFs that are < 60AA
   NAME3="c.$COUNT.$DATE"
   qsub -pe pthreads 8 -N $NAME3 -hold_jid `cat $CWD/$NAME2` -e $FINALDIR/err3 -o $FINALDIR/out3 -v CWD,FILE,FADIR $SCRIPTS/run_prodigal2.sh | sed 's/Your job //' | sed 's/ .*//' > $CWD/$NAME3

   # 4 - Rename the ORFs based on the tag and numbering scheme
   NAME4="d.$COUNT.$DATE"
   qsub -N $NAME4 -hold_jid `cat $CWD/$NAME3` -e $FINALDIR/err4 -o $FINALDIR/out4 -v CWD,FILE,DATE $SCRIPTS/run_rename.sh | sed 's/Your job //' | sed 's/ .*//' > $CWD/$NAME4
 
   # 5 - Cluster all ORFs within this set for the blastdbs (cdhit)
   NAME5="e.$COUNT.$DATE"
   qsub -pe pthreads 8 -N $NAME5 -hold_jid `cat $CWD/$NAME4` -e $FINALDIR/err5 -o $FINALDIR/out5 -v CWD,FILE $SCRIPTS/run_self-cluster.sh | sed 's/Your job //' | sed 's/ .*//' >$CWD/$NAME5

   mv $CWD/aa.* $CWD/a.* $CWD/b.* $CWD/c.* $CWD/d.* $CWD/e.* $CWD/jid
done 
   
   
   
