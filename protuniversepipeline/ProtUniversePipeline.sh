#!/bin/sh

##  This pipeline runs protein clustering using cdhit
##  And does the following:
##  1 - rename ORFs systematically (remove redundancy) 
##  2 - Cluster all ORFs within this set against current prot universe 
##  3 - self cluster any ORFs that are not part of the old prot universe

DATE="20110906"
DIR="/home/bhurwitz/bioinfo/protein_universe/create-prot-universe"
FINALDIR="/home/bhurwitz/bioinfo/protein_universe/create-prot-universe/$DATE"
FADIR="/home/bhurwitz/bioinfo/protein_universe/prot-from-selfcluster"
SCRIPTS="$FINALDIR/scripts"
cd $FINALDIR
export CWD=$PWD
export DATE=$DATE
export FADIR=$FADIR
export SCRIPTS=$SCRIPTS
export FINALDIR=$FINALDIR
export DIR=$DIR
COUNT=0

   # 1 - Rename the ORFs based on the assembler used 
   NAME1="a.$COUNT.$DATE"
   qsub -p 0 -N $NAME1 -e $FINALDIR/err1 -o $FINALDIR/out1 -v CWD,FADIR,DATE $SCRIPTS/run_rename.sh | sed 's/Your job //' | sed 's/ .*//' > $CWD/$NAME1
 
   # 2 - Cluster all ORFs within this set against current prot universe 
   #     (cdhit-2d)
   NAME2="b.$COUNT.$DATE"
   qsub -hold_jid `cat $CWD/$NAME1` -pe pthreads 24 -l vf=1.9G,h_vmem=46G,s_vmem=46G -N $NAME2 -e $FINALDIR/err2 -o $FINALDIR/out2 -v CWD,FADIR,DIR $SCRIPTS/run_pu-cluster.sh | sed 's/Your job //' | sed 's/ .*//' >$CWD/$NAME2

   # 3 - self cluster any ORFs that are not part of the old prot universe
   #     (cdhit)
   NAME3="c.$COUNT.$DATE"
   qsub -hold_jid `cat $CWD/$NAME2` -pe pthreads 24 -l vf=1.9G,h_vmem=46G,s_vmem=46G -N $NAME3 -e $FINALDIR/err3 -o $FINALDIR/out3 -v CWD $SCRIPTS/run_self-cluster.sh | sed 's/Your job //' | sed 's/ .*//' >$CWD/$NAME3 

   mv $CWD/a.* $CWD/b.* $CWD/c.* $CWD/jid
