#$ -S /bin/bash

cd $CWD

# for renaming 
OUTDIR="$CWD/results/rename-orfs"
SCRIPTS="$CWD/scripts"

# run gene finding
$SCRIPTS/rename.pl $OUTDIR $FADIR $DATE
