#! /opt/rocks/bin/perl -w

=head1 NAME

   rename.pl

=head1 SYNOPSIS

   rename.pl OUTDIR FADIR DATE 

Options:
 
   none.

=head1 DESCRIPTION

   renames ORFS using the checksum
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;
use Bio::Seq;
use Bio::SeqIO;
use Digest::MD5;

if ( @ARGV != 3 ) { die "Usage: rename.pl OUTDIR FADIR DATE\n"; }

my $outdir = shift @ARGV;
my $fadir  = shift @ARGV;
my $date   = shift @ARGV;

open( OUT, ">$outdir/combined.fa" ) || die "Cannot open fa file\n";

my @files = ();
opendir( DIR, $fadir ) or die $!;
while ( my $file = readdir(DIR) ) {
    if ( $file =~ /$date/ ) {
        push( @files, $file );
    }
}

for my $file (@files) {
    my $fa    = $fadir . "/" . $file;
    my $seqin = Bio::SeqIO->new( '-format' => 'Fasta', -file => $fa );
    my $tag   = $file;
    $tag =~ s/_.*//;

    # assign a unique identifier
    while ( ( my $seqobj = $seqin->next_seq() ) ) {
        my $id       = $seqobj->id();
        my $sequence = $seqobj->seq();
        my $name     = $date . '_' . $fa . '_' . $sequence;
        my $ctx      = Digest::MD5->new;
        $ctx->add($name);
        my $newname = $ctx->hexdigest;
        $newname = $tag . "_" . $newname;
        print OUT ">$newname\n$sequence\n";
    }
}

