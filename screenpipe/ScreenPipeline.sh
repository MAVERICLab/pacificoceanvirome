#!/bin/bash

## This pipeline runs the following steps 
## 
##  1 - convert the *sff files in a raw directory to fa/qual
##  2 - Run the qc script to remove seqs with N's / too long or short / bad qual
##  3 - Run cdhit 454 to de-replicate the data set
##  4 - Find tags in raw sequences
##  5 - Trim tags and create individual fa and qual files by tag
##  6 - Create stats to assess the pipeline

#  1 - convert the *sff files in a raw directory to fa/qual
# Date = year - month - day
# There can be multiple sff files here
DATE="20110525"
DIR="/rsgrps1/mbsulli/bioinfo/data/raw/$DATE"
FINALDIR="/rsgrps1/mbsulli/bioinfo/data/processed2/$DATE"
export FINALDIR=$FINALDIR
COUNT=0

# to find the ssinfo dir use module load roche454

cd $FINALDIR
echo "extracting sff files";
for file in `ls $DIR/*sff`; do
   COUNT=`expr $COUNT + 1`
   FA="$DATE.$COUNT.fa"
   QUAL="$DATE.$COUNT.qual"
   /uaopt/roche454/2.5.3/bin/sffinfo -s $file > $FA
   /uaopt/roche454/2.5.3/bin/sffinfo -q $file > $QUAL 
   mv $FA $FINALDIR/fasta/raw
   mv $QUAL $FINALDIR/qual/raw
done

export COUNT=$COUNT
export DATE=$DATE

##  2 - Run the qc script to remove seqs with N's / too long or short / bad qual
SCRIPTS="$PWD/scripts"
export SCRIPTS=$SCRIPTS
export CWD=$PWD
echo "running qc";
for (( i = 1 ; i <= $COUNT ; i++ ))
do
   FILE="$DATE.$i"
   export FILE=$FILE
   NAME1="a.$i.$DATE"
    FIRST=`qsub -N $NAME1 -e $FINALDIR/err -o $FINALDIR/out -v FILE,FINALDIR,SCRIPTS $SCRIPTS/run_qc.sh`
done

#  4 - Find tags
JOBS=100
export JOBS=$JOBS

# split the raw sequences into a set of sequences to run 
# through the tag finder
# we use raw so we can calc stats for the screening steps by sample

NAME2="b.$DATE"
SECOND=`qsub -W depend=afterok:$FIRST -v SCRIPTS,FINALDIR,JOBS -N $NAME2 -e $FINALDIR/err -o $FINALDIR/out $SCRIPTS/run_split.sh` 

# run the tag finding program
NAME3="c.$DATE"
THIRD=`qsub -v FINALDIR -N $NAME3 -W depend=afterok:$SECOND -e $FINALDIR/err -o $FINALDIR/out -J 1-$JOBS $SCRIPTS/run_blast_tags.sh`

# combine the ids from sequences with perfect tags and then extract out the
# fasta for these from the QCed files
# The fasta files are put in files like "mytag.fa" 
# Note these will only be those ids that passed QC

# now we need to create fasta files by tag
NAME4="d.$DATE"
FORTH=`qsub -v FINALDIR,SCRIPTS -N $NAME4 -W depend=afterok:$THIRD -e $FINALDIR/err -o $FINALDIR/out $SCRIPTS/run_sep_tags1.sh`

NAME5="e.$DATE"
FIFTH=`qsub -v FINALDIR,SCRIPTS -W depend=afterok:$FORTH -N $NAME5 -e $FINALDIR/err -o $FINALDIR/out $SCRIPTS/run_sep_tags2.sh`


NAME6="f.$DATE"
SIXTH=`qsub -N $NAME6 -W depend=afterok:$FIFTH -e $FINALDIR/err -o $FINALDIR/out -v FINALDIR,CWD,SCRIPTS $SCRIPTS/run_cdhit.sh`

# now we get the stats for the pipeline
NAME7="g.$DATE"
SEVENTH=`qsub -v FINALDIR,SCRIPTS -N $NAME7 -W depend=afterok:$SIXTH -e $FINALDIR/err -o $FINALDIR/out $SCRIPTS/run_stats.sh`

