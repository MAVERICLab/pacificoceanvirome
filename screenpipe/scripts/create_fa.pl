#!/usr/local/bin/perl

=head1 NAME
  
   create_fa.pl 

=head1 SYNOPSIS

   create_fa.pl indir fasta outdir scripts

Options:
 
   none.

=head1 DESCRIPTION

  create QCed fasta files
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;

if ( @ARGV != 4 ) { die "Usage: indir fasta outdir scripts\n"; }

my $indir   = shift @ARGV;
my $fasta   = shift @ARGV;
my $outdir  = shift @ARGV;
my $scripts = shift @ARGV;
my $outfa   = $outdir . "/" . "fasta/by-tag";
my $qcpass  = $outdir . "/" . "fasta/qcpass";
`cat $qcpass/*fa | egrep ">" | sed 's/^>//' | sed 's/ .*//' > $qcpass/qcpass.ids`;
my $qcpass_ids = $outdir . "/" . "fasta/qcpass/qcpass.ids";

opendir( DIR, $indir ) || die "Error in opening dir $indir\n";
while ( ( my $filename = readdir(DIR) ) ) {
    if ( $filename =~ /ids$/ ) {
        my $out = $filename;
        $out =~ s/\.ids//;
        my $newoutfa = $outfa . "/" . "$out.fa";
`$scripts/get_list_from_fasta.pl $qcpass_ids $indir/$filename $newoutfa $fasta`;
    }
}
closedir(DIR);

