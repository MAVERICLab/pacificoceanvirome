#!/usr/local/bin/perl

=head1 NAME

   get_stats.pl

=head1 SYNOPSIS

   get_stats.pl indir 

Options:
 
   none.

=head1 DESCRIPTION

  creates a set of stats for QCed sequences analyzed in this pipeline.
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;
use lib '/uaopt/bioperl/1.6.1/lib/perl5/site_perl/5.8.8/';
use Bio::SeqIO;
use Bio::Seq;

if ( @ARGV != 1 ) { die "Usage: get_stats.pl dir\n"; }

my $dir        = shift @ARGV;
my $derepdir   = $dir . "/fasta/dereplicate";
my $qcpassdir  = $dir . "/fasta/qcpass";
my $qcfaildir  = $dir . "/results/qc";
my $rawdir     = $dir . "/fasta/raw";
my $trimmeddir = $dir . "/fasta/by-tag";
my $tagsdir    = $dir . "/results/tags";

my $output = "$dir/stats/cleanup_stats.txt";
open( OUT, ">$output" ) || die "Cannot open out\n";

my %derep_pass;
my %qc_pass;
my %qc_fail;
my %raw_ids;
my %trimmed_pass;
my %id_to_tag;
my %tags;

# calculate stats on raw and qc results
get_derep();
get_qcpass();
get_qcfail();
get_raw();
get_trimmed();
get_tags();
calc_qc_stats();

###subs to do the work###
sub get_derep () {
    opendir DIR, "$derepdir"
      or die "Cannot open directory: $derepdir\n";
    my @files = readdir DIR;
    close DIR;

    for my $file (@files) {
        if ( $file !~ /\.fa$/ ) { next; }
        my $open = "$derepdir/$file";
        my $in   = Bio::SeqIO->new(
            -file   => $open,
            -format => 'Fasta',
        );
        while ( my $seqobj = $in->next_seq() ) {
            my $id = $seqobj->id();
            $derep_pass{$id} = $id;
        }
    }
}

sub get_qcpass () {
    opendir DIR, "$qcpassdir"
      or die "Cannot open directory: $qcpassdir\n";
    my @files = readdir DIR;
    close DIR;

    for my $file (@files) {
        if ( $file !~ /\.fa$/ ) { next; }
        my $open = "$qcpassdir/$file";
        my $in   = Bio::SeqIO->new(
            -file   => $open,
            -format => 'Fasta',
        );
        while ( my $seqobj = $in->next_seq() ) {
            my $id = $seqobj->id();
            $qc_pass{$id} = $id;
        }
    }
}

sub get_qcfail () {
    opendir DIR, "$qcfaildir"
      or die "Cannot open directory: $qcfaildir\n";
    my @files = readdir DIR;
    close DIR;

    for my $file (@files) {
        if ( $file !~ /\.stats$/ ) { next; }
        my $open = "$qcfaildir/$file";
        open( F, $open ) || die "Cannot open fail stats\n";
      LINE:
        while (<F>) {
            chomp $_;
            next LINE unless $_ =~ /\t/;
            my ( $id, $reasons ) = split( /\t/, $_ );
            $qc_fail{$id} = $reasons;
        }
    }
}

sub get_raw () {
    opendir DIR, "$rawdir"
      or die "Cannot open directory: $rawdir\n";
    my @files = readdir DIR;
    close DIR;

    for my $file (@files) {
        if ( $file !~ /\.fa$/ ) { next; }
        my $open = "$rawdir/$file";
        my $in   = Bio::SeqIO->new(
            -file   => $open,
            -format => 'Fasta',
        );
        while ( my $seqobj = $in->next_seq() ) {
            my $id = $seqobj->id();
            $raw_ids{$id} = $id;
        }
    }
}

sub get_trimmed () {
    opendir DIR, "$trimmeddir"
      or die "Cannot open directory: $trimmeddir\n";
    my @files = readdir DIR;
    close DIR;

    for my $file (@files) {
        if ( $file !~ /\.fa$/ ) { next; }
        my $open = "$trimmeddir/$file";
        my $in   = Bio::SeqIO->new(
            -file   => $open,
            -format => 'Fasta',
        );
        while ( my $seqobj = $in->next_seq() ) {
            my $id = $seqobj->id();
            $trimmed_pass{$id} = $id;
        }
    }
}

sub get_tags () {
    opendir DIR, "$tagsdir"
      or die "Cannot open directory: $tagsdir\n";
    my @files = readdir DIR;
    close DIR;

    for my $file (@files) {
        if ( $file !~ /all_tag_to_seq/ ) { next; }
        my $open = "$tagsdir/$file";
        open( F, $open ) || die "Cannot open fail stats\n";
      LINE:
        while (<F>) {
            chomp $_;
            my @fields = split( /\t/, $_ );
            my $tag    = $fields[0];
            my $id     = $fields[1];

            #next LINE if $id =~ /,/;
            $id_to_tag{$id} = $tag;
            $tags{$tag}     = $tag;
        }
    }
}

sub calc_qc_stats () {

    my %accounting;
    my @counted = (
        'total',                'pass',
        'fail',                 'contains_n',
        'length_outside_range', 'less_than_min_avg_qual',
        'less_than_100bp',      'dereplicated',
        'trimmed',              'qcpass'
    );

    #initialize accounting
    $tags{'no_tag'} = 'no_tag';
    for my $tag ( keys %tags ) {
        for my $item (@counted) {
            $accounting{$tag}{$item} = 0;
        }
    }

    # go through the raw sequences and see if / when they fell out
    # during processing
    for my $id ( keys %raw_ids ) {
        my $tag = 'no_tag';
        if ( exists $id_to_tag{$id} ) {
            $tag = $id_to_tag{$id};
        }
        $accounting{$tag}{'total'}++;
        if ( $tag ne 'no_tag' ) {
            if ( exists $derep_pass{$id} ) {
                $accounting{$tag}{'pass'}++;
            }
            else {
                $accounting{$tag}{'fail'}++;
            }
        }
        else {
            if ( !exists $qc_fail{$id} ) {
                $accounting{$tag}{'pass'}++;
            }
            else {
                $accounting{$tag}{'fail'}++;
            }
        }
        if ( exists $qc_fail{$id} ) {
            my $reasons = $qc_fail{$id};
            my @r = split( /\,/, $reasons );
            for my $reason (@r) {
                $accounting{$tag}{$reason}++;
            }
        }
        if ( !exists $derep_pass{$id} ) {
            $accounting{$tag}{'dereplicated'}++;
        }
        if ( !exists $trimmed_pass{$id} ) {
            $accounting{$tag}{'trimmed'}++;
            if ( $tag eq 'no_tag' ) {
                $accounting{$tag}{'trimmed'} = 0;
            }
        }
        if ( exists $qc_pass{$id} ) {
            $accounting{$tag}{'qcpass'}++;
        }
    }

    # print a report
    print OUT "QC stats for processing\n\n";

    #print OUT "\t", join( "\t", @counted ), "\n";
    print OUT "\ttotal\tqcpass\ttrimpass\tdereppass\n";

    for my $tag ( sort keys %accounting ) {
        my @numbers;
        push( @numbers, $tag );
        for my $item (@counted) {
            push( @numbers, $accounting{$tag}{$item} );
        }

        #print OUT join( "\t", @numbers ), "\n";
    }

    for my $tag ( sort keys %accounting ) {
        my $total     = $accounting{$tag}{'total'};
        my $qcpass    = $accounting{$tag}{'qcpass'};
        my $trimpass  = $total - $accounting{$tag}{'trimmed'};
        my $dereppass = $total - $accounting{$tag}{'dereplicated'};
        print OUT "$tag\t$total\t$qcpass\t$trimpass\t$dereppass\n";
    }

    print OUT "\n";
}
