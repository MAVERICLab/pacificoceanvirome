#!/bin/bash
### Request email when job begins and ends
#PBS -m bea

### Specify email address to use for notification.
#PBS -M bhurwitz@email.arizona.edu

### Specify the PI group found with va command
#PBS -W group_list=b5mbsaaa

### Set the queue to submit this job.
#PBS -q high_priority

### Set the memory requirements
#PBS -l mem=15Gb

### Set the number of cpus that will be used.
#PBS -l nodes=1:ppn=8

RESULTDIR="$FINALDIR/results/by-tag"
FASTA="$FINALDIR/results/tags/all_trimmed.fa"
SCRIPTS="$FINALDIR/scripts"

## now use those lists to generate fasta files
## We will only add those sequences that passed qc in the files
## also we are using the sequences that were screened for linker

$SCRIPTS/create_fa.pl $RESULTDIR $FASTA $FINALDIR $SCRIPTS

