#!/usr/local/bin/perl

=head1 NAME

    create_list_by_tag.pl

=head1 SYNOPSIS

    create_list_by_tag.pl indir outdir
Options:
 
   none.

=head1 DESCRIPTION

   create a list of ids by their sequencing tag
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;

if ( @ARGV != 2 ) { die "Usage: indir outdir\n"; }

my $indir  = shift @ARGV;
my $outdir = shift @ARGV;

my %tag_to_ids;
open( TAGSEQ, "$indir/all_tag_to_seq" ) || die "cannot open all_tag_to_seq\n";

while (<TAGSEQ>) {
    chomp $_;
    my ( $tag, $id ) = split( /\t/, $_ );
    if ( ( $tag !~ /,/ ) && ( $tag !~ /no_tag/ ) ) {
        push( @{ $tag_to_ids{$tag} }, $id );
    }
}

for my $tag ( keys %tag_to_ids ) {
    my $file = $outdir . "/" . $tag . ".ids";
    open( OUT, ">$file" ) || die "Cannot open $file\n";
    for my $id ( @{ $tag_to_ids{$tag} } ) {
        print OUT "$id\n";
    }
    close OUT;
}

