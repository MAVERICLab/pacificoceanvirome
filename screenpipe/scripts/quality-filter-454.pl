#!/usr/local/bin/perl

=head1 NAME

quality-filter-454.pl

=head1 SYNOPSIS

  quality-filter-454.pl 

  removes sequence with:
  1.  N's anywhere in the sequence
  2.  sequences whose length is > or < 1 std dev from the mean
  3.  sequences whose avg quality score is <  1 std dev from the mean
  4.  sequences that are less than 100bp 

Options:

 --help     Show brief help and exit
 --fastq    fastq file 
 --outfa    seqs that passed
 --failfa   sequences that failed
 --outqual  qual score for passing sequences
 --failqual qual scores for failing sequences
 --stats    stats on why sequences failed

=head1 DESCRIPTION

Removes problematic reads from 454 runs

=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2010 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

# ----------------------------------------------------

use strict;
use lib
'/rsgrps1/mbsulli/bioinfo/perlcode/modules/Statistics-Descriptive-Discrete-0.07/blib/lib';
use lib '/uaopt/bioperl/1.6.1/lib/perl5/site_perl/5.8.8/';
use Getopt::Long;
use Pod::Usage;
use Statistics::Descriptive::Discrete;
use Bio::SeqIO;

my ( $help, $fastq, $qual, $outfa, $failfa, $outqual, $failqual, $stats );
GetOptions(
    'h|help'     => \$help,
    'fastq=s'    => \$fastq,
    'outfa=s'    => \$outfa,
    'failfa=s'   => \$failfa,
    'outqual=s'  => \$outqual,
    'failqual=s' => \$failqual,
    'stats=s'    => \$stats
);
pod2usage(2) if $help;

my $in = Bio::SeqIO->new(
    '-file'   => "$fastq",
    '-format' => 'fastq'
);

open( STATS,      ">$stats" )    || die "cannot open stats\n";
open( FA,         ">$outfa" )    || die "cannot open fasta\n";
open( QUAL,       ">$outqual" )  || die "cannot open qual\n";
open( FAILEDFA,   ">$failfa" )   || die "cannot open fasta\n";
open( FAILEDQUAL, ">$failqual" ) || die "cannot open qual\n";

# find lengths
# find quality score by read
my @read_lengths;
my @ids;
my @seqs;
my @quals;
my @avg_qual;
my @contains_n;
while ( my $seq = $in->next_seq() ) {
    my $id = $seq->id();
    push( @ids, $id );
    my $sequence = $seq->seq();
    push( @seqs, $sequence );
    my @seq_array = split( //, $sequence );
    my $seq_length = @seq_array;
    push( @read_lengths, $seq_length );
    my $n = 0;

    for my $s (@seq_array) {
        my $uc_s = uc $s;
        if ( $uc_s eq 'N' ) {
            $n++;
        }
    }
    push( @contains_n, $n );
    my $qual = $seq->qual();
    my @qual_array;

    # we need to determine the average quality for the read
    for my $q (@$qual) {
        my $q_int = int($q);
        push( @qual_array, $q_int );
    }
    my $qual_line = join( " ", @qual_array );
    push( @quals, $qual_line );

    # get the average quality score for the read
    my $stats = Statistics::Descriptive::Discrete->new;
    $stats->add_data(@qual_array);
    my $mean_read_qual = int( $stats->mean() );
    push( @avg_qual, $mean_read_qual );
}

# now we need to find the average seq length for the set
# and the standard dev

# get stats on the mean sequence length for the whole set
my $len_stats = Statistics::Descriptive::Discrete->new;
$len_stats->add_data(@read_lengths);
my $mean_length   = int( $len_stats->mean() );
my $median_length = int( $len_stats->median() );
my $mode_length   = int( $len_stats->mode() );
my $max_length = $mean_length + ( 2 * int( $len_stats->standard_deviation() ) );
my $min_length = $mean_length - ( 2 * int( $len_stats->standard_deviation() ) );

# remove outliers and redo max and min length
my @no_outliers;
for my $len (@read_lengths) {
    if ( ( $len <= $max_length ) && ( $len >= $min_length ) ) {
        push( @no_outliers, $len );
    }
}

my $new_len_stats = Statistics::Descriptive::Discrete->new;
$new_len_stats->add_data(@no_outliers);
my $new_mean_length   = int( $new_len_stats->mean() );
my $new_median_length = int( $new_len_stats->median() );
my $new_mode_length   = int( $new_len_stats->mode() );
my $new_max_length =
  $new_mean_length + ( 2 * int( $new_len_stats->standard_deviation() ) );
my $new_min_length =
  $new_mean_length - ( 2 * int( $new_len_stats->standard_deviation() ) );

# get stats on the mean avg quality score for each read
my $avgqual_stats = Statistics::Descriptive::Discrete->new;
$avgqual_stats->add_data(@avg_qual);
my $mean_avgqual = int( $avgqual_stats->mean() );

#my $max_avgqual = $mean_avgqual + ( 2 * int( $avgqual_stats->standard_deviation() ) );
my $min_avgqual =
  $mean_avgqual - ( 2 * int( $avgqual_stats->standard_deviation() ) );

print STATS "mean_avg_qual: $mean_avgqual, min_avg_qual: $min_avgqual\n";
print STATS "median_len:$median_length\n";
print STATS "mode_len:$mode_length\n";
print STATS
  "mean_len: $mean_length, max_len: $max_length, min_len: $min_length\n";
print STATS "new_median_len:$new_median_length\n";
print STATS "new_mode_len:$new_mode_length\n";
print STATS
"new_mean_len: $new_mean_length, new_max_len: $new_max_length, new_min_len: $new_min_length\n";

my $count = 0;
for my $id (@ids) {
    my $read_length = $read_lengths[$count];
    my $seq         = $seqs[$count];
    my $qual        = $quals[$count];
    my $avg_qual    = $avg_qual[$count];
    my $contains_n  = $contains_n[$count];

    my @failed_params;

    # rule 1: seq cannot contain Ns
    if ( $contains_n > 0 ) {
        push( @failed_params, 'contains_n' );
    }

    # rule 2: remove seqs whose average quality score is less than
    #         1 std dev from the mean
    if ( $avg_qual < $min_avgqual ) {
        push( @failed_params, 'less_than_min_avg_qual' );
    }

    # rule 3: remove sequences whose length is > or <  1 std dev from the mean
    if ( ( $read_length > $max_length ) || ( $read_length < $min_length ) ) {
        push( @failed_params, 'length_outside_range' );
    }

    # rule 4: make sure the sequences are at least 100 bp
    if ( $read_length < 100 ) {
        push( @failed_params, 'less_than_100bp' );
    }

    if ( @failed_params > 0 ) {
        print FAILEDFA ">$id\n$seq\n";
        print FAILEDQUAL ">$id\n$qual\n";
        print STATS "$id\t", join( ',', @failed_params ), "\n";
    }
    else {
        print FA ">$id\n$seq\n";
        print QUAL ">$id\n$qual\n";
    }
    $count++;
}
