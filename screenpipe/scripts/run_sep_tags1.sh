#!/bin/bash
### Request email when job begins and ends
#PBS -m bea

### Specify email address to use for notification.
#PBS -M bhurwitz@email.arizona.edu

### Specify the PI group found with va command
#PBS -W group_list=b5mbsaaa

### Set the queue to submit this job.
#PBS -q high_priority

### Set the memory requirements
#PBS -l mem=15Gb

### Set the number of cpus that will be used.
#PBS -l nodes=1:ppn=8

TAGSDIR="$FINALDIR/results/tags"
RESULTDIR="$FINALDIR/results/by-tag"

cat $FINALDIR/results/tags/*.trimmed >> $FINALDIR/results/tags/all_trimmed.fa
mkdir $FINALDIR/results/tags/trimmed
mv $FINALDIR/results/tags/*.trimmed $FINALDIR/results/tags/trimmed

cat $FINALDIR/results/tags/*.masked >> $FINALDIR/results/tags/all_masked.fa
mkdir $FINALDIR/results/tags/masked
mv $FINALDIR/results/tags/*.masked $FINALDIR/results/tags/masked

cat $FINALDIR/results/tags/parse* >> $FINALDIR/results/tags/all_tag_to_seq
mkdir $FINALDIR/results/tags/tag_to_seq
mv $FINALDIR/results/tags/parse* $FINALDIR/results/tags/tag_to_seq

## create a list of ids with perfect tags for each tag
$SCRIPTS/create_list_by_tag.pl $TAGSDIR $RESULTDIR
