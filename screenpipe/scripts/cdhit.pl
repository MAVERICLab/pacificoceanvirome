#!/usr/local/bin/perl

=head1 NAME

cdhit.pl

=head1 SYNOPSIS

  cdhit.pl indir outdir scripts cwd

Options:
 
   none.

=head1 DESCRIPTION

   runs cdhit dereplication.
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;

if ( @ARGV != 4 ) { die "Usage: indir outdir scripts cwd\n"; }

my $indir   = shift @ARGV;
my $outdir  = shift @ARGV;
my $scripts = shift @ARGV;
my $cwd     = shift @ARGV;

chdir "$cwd/results/dereplicate";

my $derepdir = "$cwd/results/dereplicate";

opendir( DIR, $indir ) || die "Error in opening dir $indir\n";
while ( ( my $filename = readdir(DIR) ) ) {
    if ( $filename =~ /\.fa$/ ) {
        my $out   = $filename;
        my $outfa = $derepdir . "/" . $out;
        my $infa  = $indir . "/" . $filename;
`/rsgrps1/mbsulli/bioinfo/biotools/cd-hit-v4.5.5-2011-03-31/cd-hit-454 -i $infa -o $outfa`;
    }
}
closedir(DIR);

`cp $derepdir/*fa $outdir`;

