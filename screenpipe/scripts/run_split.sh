#!/bin/bash
### Request email when job begins and ends
#PBS -m bea

### Specify email address to use for notification.
#PBS -M bhurwitz@email.arizona.edu

### Specify the PI group found with va command
#PBS -W group_list=b5mbsaaa

### Set the queue to submit this job.
#PBS -q high_priority 

### Set the memory requirements
#PBS -l mem=2Gb

### Set the number of cpus that will be used.
#PBS -l nodes=1:ppn=1 

IN="$FINALDIR/fasta/raw"
OUT="$FINALDIR/fasta/raw/split"

$SCRIPTS/create_jobarray_fasta.pl $IN $OUT $JOBS
