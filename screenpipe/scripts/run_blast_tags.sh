#!/bin/bash
### Request email when job begins and ends
#PBS -m bea

### Specify email address to use for notification.
#PBS -M bhurwitz@email.arizona.edu

### Specify the PI group found with va command
#PBS -W group_list=b5mbsaaa

### Set the queue to submit this job.
#PBS -q high_priority

### Set the memory requirements
#PBS -l mem=2Gb

### Set the number of cpus that will be used.
#PBS -l nodes=1:ppn=1

# run program 
PROGRAM="$FINALDIR/scripts/simple_blast.pl"
QUERY="$FINALDIR/fasta/raw/split/query.$PBS_ARRAY_INDEX"
OUTPUT="$FINALDIR/results/tags/parse.$PBS_ARRAY_INDEX"
TAGS="$FINALDIR/tags.fa"
$PROGRAM $QUERY $TAGS $OUTPUT 
