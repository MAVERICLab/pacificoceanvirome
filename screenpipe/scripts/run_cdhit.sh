#!/bin/bash
### Request email when job begins and ends
#PBS -m bea

### Specify email address to use for notification.
#PBS -M bhurwitz@email.arizona.edu

### Specify the PI group found with va command
#PBS -W group_list=b5mbsaaa

### Set the queue to submit this job.
#PBS -q high_priority

### Set the memory requirements
#PBS -l mem=15Gb

### Set the number of cpus that will be used.
#PBS -l nodes=1:ppn=8

INDIR="$FINALDIR/fasta/by-tag"
FINAL="$FINALDIR/fasta/dereplicate"

# run cdhit 
$SCRIPTS/cdhit.pl $INDIR $FINAL $SCRIPTS $CWD


