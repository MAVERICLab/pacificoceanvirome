#! /usr/local/bin/perl -w

=head1 NAME

simple_blast.pl

=head1 SYNOPSIS

  simple_blast.pl [usrions]

Options:

  -h|--help     Show brief help and exit
  -v|--version  Show version and exit
  -i|--input    Input fasta file 
  -o|--output   Blast output 

=head1 DESCRIPTION

      blasts a set of input sequences in a fasta file
      against a set of blastable
      databases in a particular directory

=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2010 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

# ----------------------------------------------------

use strict;
use lib '/uaopt/bioperl/1.6.1/lib/perl5/site_perl/5.8.8/';
use Bio::SeqIO;
use Bio::PrimarySeq;
use Bio::Tools::Run::StandAloneBlast;
use Bio::Search::HSP::GenericHSP;
use Bio::Search::Hit::GenericHit;
use Bio::SeqFeature::FeaturePair;

my $TAB = "\t";
my $NL  = "\n";

my $input_file       = shift @ARGV;
my $tag_file         = shift @ARGV;
my $output_file      = shift @ARGV;
my $seq_w_n_file     = "$output_file.masked";
my $seq_trimmed_file = "$output_file.trimmed";

open my $out,     '>', $output_file  || die "Could not write output : $!\n";
open my $seq_w_n, '>', $seq_w_n_file || die "Could not write seq_w_n : $!\n";
open my $seq_trimmed, '>',
  $seq_trimmed_file || die "Could not write seq trimmed : $!\n";

# PERFORMING THE BLAST AND PARSING THE DATA:
# blasting each of the sequences against all tags

# create an object to hold all of the seq file information
my $seq_in = Bio::SeqIO->new(
    -file   => $input_file,
    -format => 'fasta'
);

while ( my $query = $seq_in->next_seq() ) {

    my $has_hit      = 0;
    my $ss           = $query->seq();
    my $q5           = $query->subseq( 1, 5 );
    my @qs           = split( //, $ss );
    my $total_length = length($ss);
    my $qid          = $query->id();

    # create an object to hold all of the tag file information
    my $tag_in = Bio::SeqIO->new(
        -file   => $tag_file,
        -format => 'fasta'
    );

    my %linker_more_than_25;
    my %coordinates_on_seq;
    while ( my $tag = $tag_in->next_seq() ) {
        my $query_name = $tag->id();
        my $sequence   = $tag->seq();
        my $t5         = $tag->subseq( 1, 5 );

        # find an exact match at the beginning to the 5 bp tag
        if ( $q5 eq $t5 ) {

            # weight an exact match heavily
            # for deciding which linker
            $linker_more_than_25{$query_name} = 100;
            for my $num ( 0 .. 4 ) {
                $coordinates_on_seq{$num} = $num;
            }
        }
        my $length = length($sequence);    # tag length
                                           # Run bl2seq on them
        my $factory =
          Bio::Tools::Run::StandAloneBlast->new( 'program' => 'blastn' );
        my $bl2seq_report = $factory->bl2seq( $tag, $query );

        # Note that report is a Bio::SearchIO object

        my $bl_result = $bl2seq_report->next_result;

        # get all hits
        while ( my $hit = $bl_result->next_hit ) {
            my $hit_name = $hit->name();

            # go through each HSP for that hit
          HSP:
            while ( my $hsp = $hit->next_hsp() ) {
                my $query_start = $hsp->start('query');
                my $query_stop  = $hsp->end('query');
                my $hit_length  = ( $query_stop - $query_start ) + 1;

                # put hit pos in order (- strand is reversed)
                my @hit_pos =
                  sort { $a <=> $b } ( $hsp->start('hit'), $hsp->end('hit') );
                my $hit_start = shift @hit_pos;
                my $hit_stop  = shift @hit_pos;

                # force at least a partial tag (minus 2 bp)
                # to make sure we aren't just assigning a read based on the
                # linker sequence alone
                if ( $hit_length >= 12 ) {
                    my $i = $hit_start - 1;
                    my $j = $hit_stop - 1;
                    for my $num ( $i .. $j ) {
                        $coordinates_on_seq{$num} = $num;
                    }
                }
                if ( $hit_length >= 20 ) {
                    if ( exists $linker_more_than_25{$query_name} ) {
                        if ( $hit_length > $linker_more_than_25{$query_name} ) {
                            $linker_more_than_25{$query_name} = $hit_length;
                        }
                    }
                    else {
                        $linker_more_than_25{$query_name} = $hit_length;
                    }
                }
            }
        }    # end of next hit
    }    # end of next tag
         #get suspected tags
    my @linkers        = ();
    my $longest_linker = 0;
    for my $tag ( keys %linker_more_than_25 ) {
        my $linker_len = $linker_more_than_25{$tag};
        if ( $linker_len > $longest_linker ) {
            $longest_linker = $linker_len;
        }
    }
    for my $tag ( keys %linker_more_than_25 ) {
        my $linker_len = $linker_more_than_25{$tag};
        if ( $linker_len == $longest_linker ) {
            push( @linkers, $tag );
        }
    }
    if ( @linkers == 0 ) {
        push( @linkers, "no_tag" );
    }
    my $linkers_list = join( ",", @linkers );
    my @fields = ( $linkers_list, $qid );
    print {$out} join( $TAB, @fields ), $NL;

    # now we will mask the sequence where the linker coord are
    my $endseq = $total_length - 1;
    for my $i ( 0 .. $endseq ) {
        if ( exists $coordinates_on_seq{$i} ) {
            $qs[$i] = "N";
        }
    }
    my $new_seq = join( "", @qs );
    print {$seq_w_n} ">$qid\n$new_seq\n";

    my $trimmed_seq = $new_seq;
    $trimmed_seq =~ s/NNGCATAC//;    # remove partial due to AA issue in linker
    $trimmed_seq =~ s/^N+//;         # remove N's at the beginning
    $trimmed_seq =~ s/N+$//;         # remove N's at the end
    $trimmed_seq =~ s/^.*NNNN//;     # remove bits at the beginning before N
    my $trimmed_length = length($trimmed_seq);
    if (   ( $trimmed_length >= 50 )
        && ( $trimmed_seq !~ /NNNNNNNNNNNNNNNNNNNN/ ) )
    {
        print {$seq_trimmed} ">$qid\n$trimmed_seq\n";
    }
}    # end of while next seq
