#!/usr/local/bin/perl

=head1 NAME

   create_fastq.pl

=head1 SYNOPSIS
   
   create_fastq.pl in.fa in.qual out.fastq

Options:
 
   none.

=head1 DESCRIPTION

   creates a fastq file from a fasta and qual file
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut


use strict;
use lib '/uaopt/bioperl/1.6.1/lib/perl5/site_perl/5.8.8/';
use Bio::SeqIO;
use Bio::Seq::Quality;

die "pass a fasta and a fasta-quality file and a fastq output file\n"
  unless @ARGV;

my $seq_infile  = $ARGV[0];
my $qual_infile = $ARGV[1];
my $fastq       = $ARGV[2];

## Create input objects for both a seq (fasta) and qual file

my $in_seq_obj = Bio::SeqIO->new(
    -file   => $seq_infile,
    -format => 'fasta',
);

my $in_qual_obj = Bio::SeqIO->new(
    -file   => $qual_infile,
    -format => 'qual',
);

my $out_fastq_obj = Bio::SeqIO->new(
    -file   => ">$fastq",
    -format => 'fastq'
);

while (1) {
    ## create objects for both a seq and its associated qual
    my $seq_obj = $in_seq_obj->next_seq || last;
    my $qual_obj = $in_qual_obj->next_seq;

    die "foo!\n"
      unless $seq_obj->id eq $qual_obj->id;

    ## Here we use seq and qual object methods feed info for new BSQ
    ## object.
    my $bsq_obj = Bio::Seq::Quality->new(
        -id   => $seq_obj->id,
        -seq  => $seq_obj->seq,
        -qual => $qual_obj->qual,
    );

    ## and print it out.
    $out_fastq_obj->write_fastq($bsq_obj);
}

`sleep 2m`;

# need to fix bioperl bug
open( F,    "$fastq" )        || die "can't open fastq\n";
open( TEMP, ">$fastq.fixed" ) || die "can't open temp\n";
my $check_next = 0;
LINE:
while (<F>) {
    if ( $_ =~ /^\+/ ) {
        $check_next++;
        print TEMP $_;
        next LINE;
    }
    if ( $check_next == 1 ) {
        my $check = $_;
        if ( $check =~ /^\@/ ) {
            $check =~ s/^\@(.*)/$1/;
            $check = "h" . $check;
        }

        #   print "$check";
        print TEMP $check;
        $check_next = 0;
        next LINE;
    }
    print TEMP $_;
}
close F;
close TEMP;

