#!/usr/local/bin/perl

=head1 NAME

   get_list_from_fasta.pl

=head1 SYNOPSIS

   get_list_from_fasta.pl qcpassids list new.fa input.fa

Options:
 
   none.

=head1 DESCRIPTION

   to get a list of sequences from a fasta file that passed qc
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;
use lib '/uaopt/bioperl/1.6.1/lib/perl5/site_perl/5.8.8/';
use Bio::SeqIO;

if ( @ARGV != 4 ) {
    die "Usage: get_list_from_fasta.pl qcpassids list new.fa input.fa\n";
}

my $qcpassids      = shift @ARGV;
my $list           = shift @ARGV;
my $outputfilename = shift @ARGV;
my $inputfilename  = shift @ARGV;

my %qcpass;
open( QCP, "$qcpassids" ) || die "Cannot open QCpass ids\n";
while (<QCP>) {
    chomp $_;
    $qcpass{$_} = $_;
}

my $in = Bio::SeqIO->new(
    '-file'   => "$inputfilename",
    '-format' => 'Fasta'
);
my $out = Bio::SeqIO->new(
    '-file'   => ">$outputfilename",
    '-format' => 'Fasta'
);

open( LIST, $list );
my %list;
while (<LIST>) {
    chomp $_;
    $list{$_} = $_;
}

# go through each of the sequence records in Genbank
while ( my $seq = $in->next_seq() ) {
    my $id = $seq->id();
    if ( exists $list{$id} ) {
        if ( exists $qcpass{$id} ) {
            $out->write_seq($seq);
        }
    }
}

