#!/bin/bash
### Request email when job begins and ends
#PBS -m bea

### Specify email address to use for notification.
#PBS -M bhurwitz@email.arizona.edu

### Specify the PI group found with va command
#PBS -W group_list=b5mbsaaa

### Set the queue to submit this job.
#PBS -q high_priority

### Set the memory requirements
#PBS -l mem=15Gb

### Set the number of cpus that will be used.
#PBS -l nodes=1:ppn=1

# run qc steps
$SCRIPTS/create_fastq.pl $FINALDIR/fasta/raw/$FILE.fa $FINALDIR/qual/raw/$FILE.qual $FINALDIR/fasta/raw/$FILE.fastq >& $FINALDIR/err/$FILE.create_fastq.err

$SCRIPTS/quality-filter-454.pl --fastq $FINALDIR/fasta/raw/$FILE.fastq.fixed --outfa $FINALDIR/fasta/qcpass/$FILE.fa --outqual $FINALDIR/qual/qcpass/$FILE.qual --failfa $FINALDIR/fasta/qcfail/$FILE.fa --failqual $FINALDIR/qual/qcfail/$FILE.qual --stats $FINALDIR/results/qc/$FILE.stats >& $FINALDIR/err/$FILE.qc.err

