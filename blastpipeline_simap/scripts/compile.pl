#!/usr/local/bin/perl

=head1 NAME

 compile.pl

=head1 SYNOPSIS

  compile.pl directory output 

Options:
 
   none.

=head1 DESCRIPTION

   This script compiles all data from indiv files in a directory
   to a single file. 
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

if ( @ARGV != 2 ) { die "Usage: compile.pl directory output\n"; }

my $dir = shift @ARGV;
my $out = shift @ARGV;

opendir DIR, "$dir"
  or die "Cannot open directory: $dir\n";
my @files = readdir DIR;
close DIR;

open( OUT, ">$out" ) || die "Cannot open outfile\n";

for my $file (@files) {
    open( F, "$dir/$file" ) || die "cannot open file\n";
    while (<F>) {
        print OUT $_;
    }
    close F;
}

close OUT;

