#!/usr/bin/perl

=head1 NAME

 split_fa.pl

=head1 SYNOPSIS

  split_fa.pl filename indir outdir jobs

Options:
 
   none.

=head1 DESCRIPTION

   This script takes a fasta file as input and splits it into
   smaller pieces for running on the cluster by job.

   #jobs is the number of pieces you want to split your file into
   should be split according to expected run time on a node (< 12 hrs).
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;

if ( @ARGV != 4 ) { die "Usage: filename indir outdir jobs\n"; }

my $filename = shift @ARGV;
$filename =~ s/.*\///;
my $indir  = shift @ARGV;
my $outdir = shift @ARGV;
my $jobs   = shift @ARGV;

my $splitdir = $outdir . "/" . $filename;
`mkdir $splitdir`;
my $infa = $indir . "/" . $filename;
`cp $infa $splitdir`;
`./scripts/create_jobarray_fasta.pl -i $splitdir -o $splitdir -j $jobs`;
