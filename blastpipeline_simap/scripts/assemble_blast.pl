#!/usr/local/bin/perl

=head1 NAME

 assemble_blast.pl

=head1 SYNOPSIS

 assemble_blast.pl indir filename 

Options:
 
   none.

=head1 DESCRIPTION

   This script compiles the blast hits for all tophits. 
   For a given indir and filename

   indir is the dir that stores the blast results (this dir)
   and filename is the file you are currently blasting.
   data will be compiled in $indir/$filename/final-out/all_tophit
 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;

if ( @ARGV != 2 ) { die "Usage: assemble_blast.pl indir filename\n"; }

my $indir    = shift @ARGV;
my $filename = shift @ARGV;
$filename =~ s/.*\///;
my $scripts = $indir . "/" . "scripts";
my $outdir  = $indir . "/" . $filename;

open( DBS, "$indir/blastdbs" ) || die "Cannot open blastdbs file\n";

while (<DBS>) {
    chomp $_;
    my ( $bldbname, $type, $blastdb ) = split( /\t/, $_ );
    my $dir = $outdir . "/" . $bldbname;
    `cd $dir`;
    my $toutdir  = $outdir . "/" . $bldbname . "/" . "tophit-out";
    my $fout     = $outdir . "/" . $bldbname . "/final-out/all_tophit";
    my $program4 = "$scripts/compile.pl";

    # compile tophits
    `$program4 $toutdir $fout`;
}

