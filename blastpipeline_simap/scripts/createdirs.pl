#!/usr/bin/perl

=head1 NAME

  createdirs.pl 

=head1 SYNOPSIS

  createdirs.pl indir file 

Options:
 
   none.

=head1 DESCRIPTION

   This script creates a set of directories necessary for running the
   blast pipeline (to hold input and output files).

   "file" is the name of the input fasta file you are blasting.  A 
   separate directory is created for each file. So you can compare
   to many different blast-able databases.

 
=head1 SEE ALSO

perl.

=head1 AUTHOR

Bonnie Hurwitz E<lt>bhurwitz@email.arizona.eduE<gt>,

=head1 COPYRIGHT

Copyright (c) 2011 Bonnie Hurwitz 

This library is free software;  you can redistribute it and/or modify 
it under the same terms as Perl itself.

=cut

use strict;

if ( @ARGV != 2 ) { die "Usage: createdirs.pl indir file\n"; }

my $indir    = shift @ARGV;
my $filename = shift @ARGV;
$filename =~ s/.*\///;
my $scripts = $indir . "/" . "scripts";
my $outdir  = $indir . "/" . $filename;

open( DBS, "$indir/blastdbs" ) || die "Cannot open blastdbs file\n";

if ( -d $outdir ) {

    # do nothing
}
else {
    `mkdir $outdir`;
}

while (<DBS>) {
    chomp $_;
    my ( $bldbname, $type, $blastdb ) = split( /\t/, $_ );
    my $dir = $outdir . "/" . $bldbname;
    `mkdir $dir`;
    `mkdir $dir/blast-out`;
    `mkdir $dir/final-out`;
    `mkdir $dir/parse-out`;
    `mkdir $dir/tophit-out`;
}

