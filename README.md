# The Pacific Ocean Virome (POV) #

## Author ##

Bonnie Hurwitz

## CITATION ##

If you use this software, please cite [Hurwitz, B.L., & Sullivan, M.B. (2013). The Pacific Ocean Virome (POV): A marine viral metagenomic dataset and associated protein clusters for quantitative viral ecology. PLoS One. 8(2), e57355.](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0057355)